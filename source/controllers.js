const data = require('./data.js');

exports.hello = (req, res) => {
  res.send('Hello World fron controller');
}

exports.customersOrderedByPurchases = (req, res) => {
  res.setHeader('Content-Type', 'application/json');

  data.purchases()
    .then((purchases) => {
      let sumared = {};

      purchases.forEach((purchase) => {
        let cliente = purchase.cliente.substr(-2);

        if (sumared[cliente]) {
          sumared[cliente].valorTotal += purchase.valorTotal;
        } else {
          sumared[cliente] = {
            valorTotal: purchase.valorTotal
          };
        }
      });

      return sumared;
    })
    .then((sumared) => {
      let wait = [];

      Object.keys(sumared).forEach((key) => {
        let len = wait.push(data.clientByFinalCpf(key));

        wait[len-1].then((client) => {
          sumared[key] = Object.assign(client, sumared[key]);
        });
      });

      return Promise.all(wait).then(() => {
        return sumared;
      });
    })
    .then((clients) => {
      let orderedKeys    = Object.keys(clients);
      let ordenedClients = [];

      orderedKeys.sort((a, b) => {

        if (clients[a].valorTotal > clients[b].valorTotal) {
          return -1;
        } else if (clients[a].valorTotal < clients[b].valorTotal) {
          return 1;
        }

        return 0;
      });

      orderedKeys.forEach((key) => {
        ordenedClients.push(clients[key]);
      });

      return ordenedClients;
    })
    .then((orderedClients) => {
      res.send(JSON.stringify(orderedClients));
    });
};

exports.uniquePurchase2016 = (req, res) => {
  res.setHeader('Content-Type', 'application/json');

  data.purchases()
    .then((purchases) => {
      let purchase = {
        valorTotal : 0
      };

      purchases.forEach((item) => {
        let year = item.data.split('-')[2];

        if (year === '2016' && (item.valorTotal > purchase.valorTotal)) {
          purchase = item;
        }
      });

      return purchase;
    })
    .then((purchase) => {
      let clientKey = purchase.cliente.substr(-2);

      return data.clientByFinalCpf(clientKey).then((client) => {
        client.maiorCompra = purchase;

        return client;
      });
    })
    .then((clients) => {
      res.send(JSON.stringify(clients));
    });
};

exports.customersMostFaithful = (req, res) => {
  res.setHeader('Content-Type', 'application/json');

  data.purchases()
    .then((purchases) => {
      let sumared = {};

      purchases.forEach((purchase) => {
        let cliente = purchase.cliente.substr(-2);

        if (sumared[cliente]) {
          sumared[cliente].totalCompras++;
        } else {
          sumared[cliente] = {
            totalCompras: 1
          };
        }
      });

      return sumared;
    })
    .then((sumared) => {
      let orderedKeys    = Object.keys(sumared);
      let ordenedClients = {};

      orderedKeys.sort((a, b) => {
        if (sumared[a].totalCompras > sumared[b].totalCompras) {
          return -1;
        } else if (sumared[a].totalCompras < sumared[b].totalCompras) {
          return 1;
        }

        return 0;
      });

      orderedKeys = orderedKeys.slice(0, 3);

      orderedKeys.forEach((key) => {
        ordenedClients[key] = sumared[key];
      });

      return ordenedClients;
    })
    .then((ordered) => {
      let wait = [];

      Object.keys(ordered).forEach((key) => {
        let len = wait.push(data.clientByFinalCpf(key));

        wait[len-1].then((client) => {
          ordered[key] = Object.assign(client, ordered[key]);
        });
      });

      return Promise.all(wait).then(() => {
        return Object.values(ordered);
      });
    })
    .then((orderedClients) => {
      res.send(JSON.stringify(orderedClients));
    });
};
