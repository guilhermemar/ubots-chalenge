const express     = require('express');
const controllers = require('./controllers');

const app  = express();
const port = 3000;

app.get('/', controllers.hello);
app.get('/customers-ordered-by-purchases', controllers.customersOrderedByPurchases);
app.get('/unique-purchase-2016', controllers.uniquePurchase2016);
app.get('/customers-most-faithful', controllers.customersMostFaithful);

app.listen(port, () => console.log(`Running on port ${port}!`))
