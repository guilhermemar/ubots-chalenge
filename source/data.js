const fetch = require('node-fetch');

let dataCustomers = null,
    dataPurchases = null;

// private methods

let customers = () => {
  return new Promise ((resolve) => {
    if (dataCustomers === null) {
      fetch('http://www.mocky.io/v2/598b16291100004705515ec5')
        .then((data) => {
          dataCustomers = data.json();
          resolve(dataCustomers)
        });
    } else {
      resolve(dataCustomers);
    }
  });
}

let purchases = () => {
  return new Promise ((resolve) => {
    if (dataPurchases === null) {
      fetch('http://www.mocky.io/v2/598b16861100004905515ec7')
        .then((data) => {
          dataPurchases = data.json();
          resolve(dataPurchases)
        });
    } else {
      resolve(dataPurchases);
    }
  });
}

// public methods

exports.customers = customers;
exports.purchases = purchases;

exports.customerById = (id) => {
  return customers()[id];
};

exports.clientByFinalCpf = (finalCpf) => {
  let key = Number(finalCpf) -1;

  return new Promise((resolve) => {
    customers().then((customers) => {
      resolve(customers[key]);
    });
  });

};
