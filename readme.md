# ubots chalenge

Problema #1: Venda de Vinhos
Velasquez possui uma loja de vinhos e, ao longo dos anos, guardou dados de seus
clientes e um histórico de compras. Velasquez quer personalizar o atendimento e
contratou você para desenvolver um software que:

- [x] 1 - Liste os clientes ordenados pelo maior valor total em compras.
- [x] 2 - Mostre o cliente com maior compra única no último ano (2016).
- [x] 3 - Liste os clientes mais fiéis.
- [ ] 4 - Recomende um vinho para um determinado cliente a partir do histórico de compras.

Para criar esse software o neto do Velasquez (o Velasquinho) disponibilizou uma
API com cadastro de clientes
(http://www.mocky.io/v2/598b16291100004705515ec5) e histórico de compras
(http://www.mocky.io/v2/598b16861100004905515ec7).

## Implementation

* __npm install__ - Instala dependencias
* __npm start__ - Roda o sistema

### routes

* __/customers-ordered-by-purchases__ - Lista os clientes ordenados pelo maior valor de compras no total
* __/unique-purchase-2016__ - Retorna o cliente com maior compra única em 2016
* __/customers-most-faithful__ - Lista os 3 clientes que mais compraram
